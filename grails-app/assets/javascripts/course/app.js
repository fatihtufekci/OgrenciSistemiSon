var app = angular.module("myApp",[]);

app.controller("Fatih",function ($scope, $rootScope, $http, $location, $timeout, $window){
    var myVm = this;
    myVm.getUsername = getUsername;

    myVm.getUsername();

    function getUsername() {
        $http.get("http://localhost:8090/anasayfa/deneme").then(function (response) {
            myVm.fatih = response.data.username
        });
    }

});
app.controller("AnnouncementController",function ($scope, $rootScope, $http, $location, $timeout, $window){
    var myVm = this;
    myVm.insert = insert;
    myVm.getDepartmentList = getDepartmentList;

    myVm.getDepartmentList();

    function insert() {
        $http.post("http://localhost:8090/person/addAnnouncement", data={annoInstance:myVm.annoInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("Insert is a successfully");
                    myVm.annoInstance = {};
                }else{
                    alert("WRONG..!!");
                }
            });
    }


    function getDepartmentList() {
        $http.get("http://localhost:8090/department/getir").then(function (response) {
            myVm.department = response.data;
        });
    }

});
app.controller("StudentController",function ($scope, $rootScope, $http, $location, $timeout, $window){
    var myVm = this;
    myVm.insert = insert;
    myVm.getDepartmentList = getDepartmentList;

    myVm.getDepartmentList();

    function insert() {
        $http.post("http://localhost:8090/person/addStudent", data={studentInstance:myVm.studentInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("Insert is a successfully");
                    myVm.studentInstance = {};
                }else{
                    alert("WRONG..!!");
                }
            });
    }


    function getDepartmentList() {
        $http.get("http://localhost:8090/department/getir").then(function (response) {
            myVm.department = response.data;
        });
    }

});
app.controller("TeacherController",function ($scope, $rootScope, $http, $location, $timeout, $window){
    var myVm = this;
    myVm.insert = insert;
    myVm.getDepartmentList = getDepartmentList;

    myVm.getDepartmentList();

    function insert() {
        $http.post("http://localhost:8090/person/addTeacher", data={teacherInstance:myVm.teacherInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("Insert is a successfully");
                    myVm.teacherInstance = {};
                }else{
                    alert("WRONG..!!");
                }
            });
    }


    function getDepartmentList() {
        $http.get("http://localhost:8090/department/getir").then(function (response) {
            myVm.department = response.data;
        });
    }

});
app.controller("CourseController", function ($scope, $rootScope, $http, $location, $timeout, $window) {

    var myVm = this;

    myVm.insert = insert;
    myVm.getDepartmentList = getDepartmentList;
    myVm.getStatusList = getStatusList;
    myVm.getTermList = getTermList;
    myVm.getTypeList = getTypeList;
    myVm.getTeacherList = getTeacherList;

    myVm.getTeacherList();
    myVm.getDepartmentList();
    myVm.getStatusList();
    myVm.getTermList();
    myVm.getTypeList();



    function insert() {
        $http.post("http://localhost:8090/course/addCourse", data={courseInstance:myVm.courseInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("Insert is a successfully");
                    myVm.courseInstance = {};
                }else{
                    alert("WRONG..!!");
                }
            });
    }




    function getTeacherList() {
        $http.get("http://localhost:8090/person/getir").then(function (response) {
            myVm.teachers = response.data;
        });
    }

    function getDepartmentList() {
        $http.get("http://localhost:8090/department/getir").then(function (response) {
            myVm.department = response.data;
        });
    }

    function getStatusList() {
        $http.get("http://localhost:8090/status/getir").then(function (response) {
            myVm.status = response.data;
        });
    }

    function getTermList() {
        $http.get("http://localhost:8090/term/getir").then(function (response) {
            myVm.term = response.data;
        });
    }

    function getTypeList() {
        $http.get("http://localhost:8090/type/getir").then(function (response) {
            myVm.type = response.data;
        });
    }

});
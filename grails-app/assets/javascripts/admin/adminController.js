app.controller("AdminController", function ($scope, $rootScope, $http) {
    var myVm = this;

    myVm.getUsername = getUsername;
    myVm.btnStaffAction = btnStaffAction;
    myVm.btnCourseAction = btnCourseAction;
    myVm.btnStudentAction = btnStudentAction;
    myVm.btnTeacherAction = btnTeacherAction;
    myVm.btnAnnoAction = btnAnnoAction;
    myVm.getannoList = getannoList;
    myVm.editcourse = editcourse;
    myVm.deletecourse = deletecourse;
    myVm.getcourseinfo = getcourseinfo;
    myVm.getcourses = getcourses;
    myVm.getPersonStudent = getPersonStudent;
    myVm.getPersonTeacher = getPersonTeacher;
    myVm.getannoinfo = getannoinfo;
    myVm.getDepartmentList = getDepartmentList;
    myVm.editanno = editanno;
    myVm.deleteanno = deleteanno;
    myVm.editteacher = editteacher;
    myVm.deleteteacher = deleteteacher;
    myVm.getteacherinfo = getteacherinfo;
    myVm.editstudent = editstudent;
    myVm.deletestudent = deletestudent;
    myVm.getstudentinfo = getstudentinfo;
    myVm.getStatusList = getStatusList;
    myVm.getTermList = getTermList;
    myVm.getTypeList = getTypeList;


    myVm.getUsername();
    myVm.getannoList();

    myVm.getTermList();
    myVm.getTypeList();
    myVm.getStatusList();
    myVm.getDepartmentList();
    myVm.getPersonStudent();
    myVm.getPersonTeacher();
    myVm.getcourses();

    myVm.btnCourse = false;
    myVm.btnStaff = false;
    myVm.btnStudent = false;
    myVm.btnTeacher = false;
    myVm.btnAnnouncement = false;
    myVm.btnEditanno = false;
    myVm.btnEditTeacher = false;
    myVm.btnEditStudent = false;
    myVm.btnEditcourse = false;

    function getstudentinfo(studentId, flag) {
        $http.post("http://localhost:8090/admin/getstudentinfo", data={id:studentId})
            .then(function (response) {
                myVm.studentInstance = response.data.studentInstance;
            });
        if(flag){
            myVm.btnEditStudent = false;
        }else{
            myVm.btnEditStudent = true;
        }
    }

    function editstudent() {
        $http.post("http://localhost:8090/admin/editstudent", data={personStudentInstance:myVm.studentInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("İşlem Başarılı..");
                    myVm.studentInstance = {};
                    myVm.btnEditStudent = false;
                    myVm.getPersonStudent();
                }else if (response.data.messageType == "error") {
                }
            });
    }

    function deletestudent(personStudentId) {
        $http.post("http://localhost:8090/admin/deletestudent", data={personStudentId:personStudentId})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    myVm.getPersonStudent();
                }
            });
    }

    function getDepartmentList() {
        $http.get("http://localhost:8090/department/getir")
            .then(function (response) {
                myVm.departments = response.data;
            });
    }

    function getteacherinfo(teacherId, flag) {
        $http.post("http://localhost:8090/admin/getteacherinfo", data={id:teacherId})
            .then(function (response) {
                myVm.teacherInstance = response.data.teacherInstance;
            });
        if(flag){
            myVm.btnEditTeacher = false;
        }else{
            myVm.btnEditTeacher = true;
        }
    }

    function editteacher() {
        $http.post("http://localhost:8090/admin/editteacher", data={personTeacherInstance:myVm.teacherInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("İşlem Başarılı..");
                    myVm.teacherInstance = {};
                    myVm.btnEditTeacher = false;
                    myVm.getPersonTeacher();
                }else if (response.data.messageType == "error") {
                }
            });
    }
    
    function deleteteacher(personTeacherId) {
        $http.post("http://localhost:8090/admin/deleteteacher", data={personTeacherId:personTeacherId})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    myVm.getPersonTeacher();
                }
            });
    }
    
    function getannoinfo(annoId, flag) {
        $http.post("http://localhost:8090/admin/getannoinfo", data={id:annoId})
            .then(function (response) {
                myVm.annoInstance = response.data.annoInstance;
            });
        if(flag){
            myVm.btnEditanno = false;
        }else{
            myVm.btnEditanno = true;
        }
    }

    function deleteanno(annoId) {
        $http.post("http://localhost:8090/admin/deleteanno", data={annoId:annoId})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    myVm.getannoList();
                }else{
                    alert("İşlem Başarısız..!!!");
                }
            });
    }

    function editanno() {
        $http.post("http://localhost:8090/admin/editanno", data={annoInstance:myVm.annoInstance})
            .then(function (response) {
                if (response.data.messageType == "info"){
                    alert("İşlem Başarılı..");
                    myVm.annoInstance = {};
                    myVm.btnEditanno = false;
                    myVm.getannoList();
                }else if (response.data.messageType == "error") {
                }
            });
    }

    function getcourseinfo(courseId, flag) {
        $http.post("http://localhost:8090/admin/getcourseinfo", data={id:courseId})
            .then(function (response) {
                myVm.courseInstance = response.data.courseInstance;
            });
        if(flag){
            myVm.btnEditcourse = false;
        }else{
            myVm.btnEditcourse = true;
        }
    }

    function deletecourse() {
        $http.post("http://localhost:8090/admin/deletecourse", data={courseInstance:myVm.courseInstance})
            .then(function (response) {

            });
    }

    function editcourse() {
        $http.post("http://localhost:8090/admin/editcourse", data={courseInstance:myVm.courseInstance})
            .then(function (response) {
                alert("İşlem Başarılı..");
                myVm.courseInstance = {};
                myVm.btnEditcourse = false;
                myVm.getcourses();
            });
    }

    function getStatusList() {
        $http.get("http://localhost:8090/status/getir").then(function (response) {
            myVm.status = response.data;
        });
    }

    function getTermList() {
        $http.get("http://localhost:8090/term/getir").then(function (response) {
            myVm.term = response.data;
        });
    }

    function getTypeList() {
        $http.get("http://localhost:8090/type/getir").then(function (response) {
            myVm.type = response.data;
        });
    }

    function getPersonStudent(){
        $http.get("http://localhost:8090/person/getPersonStudent")
            .then(function (response) {
                myVm.personStudent = response.data;
            });
    }

    function getPersonTeacher(){
        $http.get("http://localhost:8090/person/getPersonTeacher")
            .then(function (response) {
                myVm.personTeacher = response.data;
            });
    }

    function getcourses() {
        $http.get("http://localhost:8090/studenthome/getcourses")
            .then(function (response) {
                myVm.courses = response.data;
            });
    }

    function getannoList() {
        $http.get("http://localhost:8090/studenthome/getannolist")
            .then(function (response) {
                myVm.announcement = response.data;
            });
    }

    function btnStudentAction(flag) {
        if(flag){
            myVm.btnCourse = false;
            myVm.btnStaff = false;
            myVm.btnStudent = false;
            myVm.btnTeacher = false;
            myVm.btnAnnouncement = false;
        }else{
            myVm.btnStudent = true;
        }
    }
    function btnTeacherAction(flag) {
        if(flag){
            myVm.btnCourse = false;
            myVm.btnStaff = false;
            myVm.btnStudent = false;
            myVm.btnTeacher = false;
            myVm.btnAnnouncement = false;
        }else{
            myVm.btnTeacher = true;
        }
    }
    function btnAnnoAction(flag) {
        if(flag){
            myVm.btnCourse = false;
            myVm.btnStaff = false;
            myVm.btnStudent = false;
            myVm.btnTeacher = false;
            myVm.btnAnnouncement = false;
        }else{
            myVm.btnAnnouncement = true;
        }
    }
    function btnCourseAction(flag){
        if(flag){
            myVm.btnCourse = false;
            myVm.btnStaff = false;
            myVm.btnStudent = false;
            myVm.btnTeacher = false;
            myVm.btnAnnouncement = false;
        }else{
            myVm.btnCourse = true;
        }
    }
    function btnStaffAction(flag){
        if (flag) {
            myVm.btnStaff = false;
            myVm.btnCourse = false;
            myVm.btnStudent = false;
            myVm.btnTeacher = false;
            myVm.btnAnnouncement = false;
        } else {
            myVm.btnStaff = true;
        }
    }

    function getUsername() {
        $http.get("http://localhost:8090/anasayfa/deneme").then(function (response) {
            myVm.fatih = response.data.username
        });
    }

});
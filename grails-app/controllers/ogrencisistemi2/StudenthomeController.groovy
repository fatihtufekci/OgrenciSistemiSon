package ogrencisistemi2

import com.fatih.Announcement
import com.fatih.Course
import com.fatih.CourseStudent
import com.fatih.CourseTeacher
import com.fatih.auth.User
import grails.converters.JSON
import grails.gorm.transactions.Transactional

class StudenthomeController {

    AnnouncementService announcementService
    CourseService courseService
    CourseStudentService courseStudentService
    CourseTeacherService courseTeacherService
    DepartmentService departmentService
    TypeService typeService
    StatusService statusService
    TermService termService

    def springSecurityService

    def index() {

    }

    @Transactional
    def selectcourses(){
        def currentUser=(User)springSecurityService.getCurrentUser()
        def person = currentUser.person
        def resultMap
        if(person!=null){
            def personStudent = person.student

            def courseId = request.JSON.id
            def courseInstance
            if(courseId != null){
                courseInstance = courseService.get(courseId)
                def courseStudent = new CourseStudent()
                courseStudent.student = personStudent
                courseStudent.course = courseInstance
                courseStudentService.save(courseStudent)

                if(courseStudent.errors?.errorCount > 0){
                    resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
                }else{
                    resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
                }

                def courseTeacherList = CourseTeacher.getAll()

            }else{
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }

    def getannolist(){
        def currentUser=(User)springSecurityService.getCurrentUser()
        def person = currentUser.person
        def a = person?.student?.department

        def list = Announcement.withCriteria {
            Date now = new Date()
            //eq("departments", a)
            le("releaseDate", now)
            ge("dateRemovedFromBroadcast", now)
        }
        def resultArray = []
        list.collect{
            resultArray.add(it.convertToMap())
        }
        render resultArray as JSON
    }

    def gettype(){
        def list = typeService.list(params)
        render list as JSON
    }

    def getterm(){
        def list = termService.list(params)
        render list as JSON
    }

    def getstatus(){
        def list = statusService.list(params)
        render list as JSON
    }

    def getdepartment(){
        def list = departmentService.list(params)
        render list as JSON
    }

    def getMycourses(){
        def currentUser=(User)springSecurityService.getCurrentUser()
        def person = currentUser.person
        def student = person?.student
        def list
        if(student!=null){
            list = CourseStudent.createCriteria().list {
                eq("student", student)
            }
        }else{
            list = courseStudentService.list(params)
        }
        def resultArray = []
        list.collect{
            resultArray.add(it.convertToMap())
        }
        render list as JSON
    }

    def getcourses(){
        def currentUser=(User)springSecurityService.getCurrentUser()
        def person = currentUser.person
        def a = person?.student?.department
        def list2
        if(a!=null){
            list2 = Course.createCriteria().list {
                eq("department", a)
            }
        }else{
            list2 = courseService.list(params)
        }


        //def list = courseService.list(params)
        def resultArray = []
        /*list.collect{
            resultArray.add(it.convertToMap())
        }*/
        list2.collect{
            resultArray.add(it.convertToMap())
        }
        render resultArray as JSON
    }
}

package ogrencisistemi2

import grails.converters.JSON

class TypeController {

    TypeService typeService

    def index() { }

    def getir(){
        def liste = typeService.list(params)
        render liste as JSON
    }
}

package ogrencisistemi2

import com.fatih.Announcement
import com.fatih.Department
import com.fatih.Person
import com.fatih.Student
import com.fatih.Teacher
import com.fatih.Usertype
import com.fatih.auth.Role
import com.fatih.auth.User
import com.fatih.auth.UserRole
import grails.converters.JSON
import grails.gorm.transactions.Transactional

import java.text.SimpleDateFormat

class PersonController {

    PersonService personService
    TeacherService teacherService
    StudentService studentService
    AnnouncementService announcementService

    def index() { }

    def addAnnouncement(){
        def annoJson = request.JSON.annoInstance
        def annoInstance
        def resultMap

        if(annoJson != null){
            annoInstance = new Announcement()
            annoInstance.properties = annoJson
            annoJson.departments.each{
                def departmentInstance = Department.read(it)
                annoInstance.removeFromDepartments(departmentInstance)
                annoInstance.addToDepartments(departmentInstance)
            }
            announcementService.save(annoInstance)
            if(annoInstance.errors?.errorCount>0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }
        render resultMap as JSON
    }

    @Transactional
    def addStudent(){
        def studentJson = request.JSON.studentInstance
        def studentInstance
        def personInstance
        def resultMap

        if(studentJson != null){
            personInstance = new Person()
            studentInstance = new Student()
            personInstance.properties = studentJson
            studentInstance.properties = studentJson
            //studentInstance.department = studentJson.department
            studentService.save(studentInstance)
            personInstance.student = studentInstance
            personService.save(personInstance)

            def admin = Role.findByAuthority("ROLE_USER")
            def typeadmin = Usertype.findById(2)
            def email = personInstance.firstName.toLowerCase() + "."+personInstance.lastName.toLowerCase()+"@izu.edu.tr"

            if(!User.findByUsername(email)){
                def user = new User(username: email, password: "gs", usertype: typeadmin, person: personInstance).save()
                UserRole.create(user, admin, false)
            }


            if(personInstance.errors?.errorCount>0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }

            if(studentInstance.errors?.errorCount>0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }
        render resultMap as JSON
    }

    def addTeacher(){
        def teacherJson = request.JSON.teacherInstance
        def teacherInstance
        def personInstance
        def resultMap

        if(teacherJson != null){
            personInstance = new Person()
            teacherInstance = new Teacher()

            // Kaydederken tckn ve phoneNumber alanlarının sayı olduğuna dikkat edin.
            // Eğer sayı olmassa hata verir.
            // personInstance.tckn = teacherJson.tckn
            personInstance.properties = teacherJson
            teacherInstance.properties = teacherJson

            teacherService.save(teacherInstance)

            personInstance.teacher = teacherInstance
            personService.save(personInstance)

            if(personInstance.errors?.errorCount>0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }

            if(teacherInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }

        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }
    
    def getpersoninfo(){
        def personId = -1
        personId = request.JSON.id
        def resultMap
        if(personId != -1){

        }
    }

    def getir(){
        def liste = Person.withCriteria {
            isNotNull("teacher")
        }
        render liste as JSON
    }

    def teachergetir(){
        def liste = Teacher.withCriteria {
            isNotNull("teacher")
        }
        render liste as JSON
    }

    def getPersonTeacher(){
        def liste = Person.withCriteria {
            isNotNull("teacher")
        }
        def resultArray = []
        liste.collect{
            resultArray.add(it.convertToMap())
        }
        render resultArray as JSON
    }

    def getPersonStudent(){
        def liste = Person.withCriteria {
            isNotNull("student")
        }
        def resultArray = []
        liste.collect{
            resultArray.add(it.convertToMap())
        }

        render resultArray as JSON
    }
}

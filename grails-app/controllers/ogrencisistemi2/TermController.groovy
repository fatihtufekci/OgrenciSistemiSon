package ogrencisistemi2

import grails.converters.JSON

class TermController {

    TermService termService

    def index() { }

    def getir(){
        def liste = termService.list(params)
        render liste as JSON
    }
}

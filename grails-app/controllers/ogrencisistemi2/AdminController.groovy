package ogrencisistemi2

import com.fatih.Department
import grails.converters.JSON

class AdminController {

    AnnouncementService announcementService
    TeacherService teacherService
    PersonService personService
    DepartmentService departmentService
    CourseService courseService

    def index() { }

    def getannoinfo(){
        def annoId = request.JSON.id
        def resultMap
        def annoInstance
        if(annoId != null){
            annoInstance = announcementService.get(annoId)
            if(annoInstance!=null){
                resultMap = [messageType:"info", message:"İşlem Başarıyla Gerçekleşti", annoInstance:annoInstance?.convertToMap()]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!", annoInstance:null]
        }
        render resultMap as JSON
    }

    def editanno(){
        def annoJson = request.JSON.annoInstance
        def annoInstance
        def resultMap

        if(annoJson != null){
            if(annoJson.id){
                annoInstance = announcementService.get(annoJson.id)
                annoInstance.properties = annoJson
            }
        }
        annoInstance.departments = annoJson.departments
        annoInstance.departments.clear()

        if(annoInstance.validate()){
            annoJson.departments.each{
                def departmentInstance = Department.read(it)
                annoInstance.removeFromDepartments(departmentInstance)
                annoInstance.addToDepartments(departmentInstance)
            }
            announcementService.save(annoInstance)

            if(annoInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }

    def deleteanno(){
        def annoId = request.JSON.annoId

        def resultMap
        // Bağlantılı tablolarda'da delete işlemini yap.
        if(annoId != null){
            try {
                announcementService.delete(annoId)
                resultMap = [messageType:"info", message:"İşlem Başarılı"]
            }catch (Exception e){
                resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
            }

        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
        }
        render resultMap as JSON
    }
    
    def getteacherinfo(){
        def teacherId = request.JSON.id
        def resultMap
        def teacherInstance
        if(teacherId != null){
            teacherInstance = personService.get(teacherId)
            if(teacherInstance!=null){
                resultMap = [messageType:"info", message:"İşlem Başarıyla Gerçekleşti", teacherInstance:teacherInstance?.convertToMap()]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!", teacherInstance:null]
        }
        render resultMap as JSON
    }

    def getstudentinfo(){
        def studentId = request.JSON.id
        def resultMap
        def studentInstance
        if(studentId != null){
            studentInstance = personService.get(studentId)
            if(studentInstance!=null){
                resultMap = [messageType:"info", message:"İşlem Başarıyla Gerçekleşti", studentInstance:studentInstance?.convertToMap()]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!", studentInstance:null]
        }
        render resultMap as JSON
    }
        
    def editstudent(){
        def personStudentJson = request.JSON.personStudentInstance
        def personStudentInstance
        def resultMap

        if(personStudentJson != null){
            if(personStudentJson.id){
                personStudentInstance = personService.get(personStudentJson.id)
                personStudentInstance.properties = personStudentJson
                def sgn = String.valueOf(personStudentJson.student.studentGhostNo)
                personStudentInstance.student.studentNo = Long.parseLong(sgn)
                def sgc = String.valueOf(personStudentJson.student.studentGhostClasses)
                personStudentInstance.student.classes = Integer.parseInt(sgc)
                def departmentInstance = departmentService.get(personStudentJson.student.studentGhostDepartmentId)
                personStudentInstance.student.department = departmentInstance
            }
        }
        if(personStudentInstance.validate()){
            personService.save(personStudentInstance)

            if(personStudentInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }
    
    def editteacher(){
        def personTeacherJson = request.JSON.personTeacherInstance
        def personTeacherInstance
        def resultMap

        if(personTeacherJson != null){
            if(personTeacherJson.id){
                personTeacherInstance = personService.get(personTeacherJson.id)
                personTeacherInstance.properties = personTeacherJson
                personTeacherInstance.teacher.educationLevel = personTeacherJson.teacher.teacherGhostEducationLevel
                def departmentInstance = departmentService.get(personTeacherJson.teacher.teacherGhostDepartmentId)
                personTeacherInstance.teacher.department = departmentInstance
            }
        }
        if(personTeacherInstance.validate()){
            personService.save(personTeacherInstance)

            if(personTeacherInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }
    
    def deletestudent(){
        def personStudentId = request.JSON.personStudentId
        def resultMap

        // Bağlantılı tablolarda'da delete işlemini yap.
        if(personStudentId != null){
            personService.delete(personStudentId)
            resultMap = [messageType:"info", message:"İşlem Başarılı"]
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
        }
        render resultMap as JSON
    }
    
    def deleteteacher(){
        def personTeacherId = request.JSON.personTeacherId
        def resultMap

        // Bağlantılı tablolarda'da delete işlemini yap.
        if(personTeacherId != null){
            personService.delete(personTeacherId)
            resultMap = [messageType:"info", message:"İşlem Başarılı"]
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
        }
        render resultMap as JSON
    }
    
    def getcourseinfo(){
        def courseId = request.JSON.id
        def resultMap
        def courseInstance
        if(courseId != null){
            courseInstance = courseService.get(courseId)
            if(courseInstance!=null){
                resultMap = [messageType:"info", message:"İşlem Başarıyla Gerçekleşti", courseInstance:courseInstance?.convertToMap()]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!", annoInstance:null]
        }
        render resultMap as JSON
    }
    
    def editcourse(){
        def courseJson = request.JSON.courseInstance
        def courseInstance
        def resultMap

        if(courseJson != null){
            if(courseJson.id){
                courseInstance = courseService.get(courseJson.id)
                courseInstance.properties = courseJson
            }
        }
        if(courseInstance.validate()){
            courseService.save(courseInstance)

            if(courseInstance.errors?.errorCount > 0){
                resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
            }else{
                resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }

    def deletecourse(){
        def courseId = request.JSON.courseId
        def resultMap

        // Bağlantılı tablolarda'da delete işlemini yap.
        if(courseId != null){
            personService.delete(courseId)
            resultMap = [messageType:"info", message:"İşlem Başarılı"]
        }else{
            resultMap = [messageType:"error", message:"İşlem Başarısız.!!"]
        }
        render resultMap as JSON
    }
}

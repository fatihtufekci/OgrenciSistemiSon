package ogrencisistemi2

import grails.converters.JSON

class StatusController {

    StatusService statusService

    def index() { }

    def getir(){
        def liste = statusService.list(params)
        render liste as JSON
    }
}

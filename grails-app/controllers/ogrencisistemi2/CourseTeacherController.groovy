package ogrencisistemi2

import com.fatih.CourseTeacher
import com.fatih.Teacher
import grails.converters.JSON

class CourseTeacherController {

    CourseTeacherService courseTeacherService

    def index() { }

    def add(){
        def courseInstance = request.JSON.courseInstance
        def courseTeacher = request.JSON.courseTeacher

        def resultMap
        def courseTeacherInstance

        if(courseTeacher != null && courseId != null){
            courseTeacherInstance = new CourseTeacher()
            courseTeacherInstance.course.id = courseId
            courseTeacher.courseTeachers.value.each{
                def teacherInstance = Teacher.read(it)
                courseTeacherInstance.teacher.addToId(teacherInstance)
            }
            courseTeacherService.save(courseTeacherInstance)
            resultMap = [messageType:"info", message:"İşlem başarıyla gerçekleşti."]
        }else{
            resultMap = [messageType:"error", message:"İşlem başarısız!!."]
        }

       /*

        if(courseTeacherJson!=null){
            courseTeacherInstance = new CourseTeacher()
            courseTeacherInstance.courseId = courseTeacherJson.id
            courseTeacherJson.teacher.each{
                courseTeacherInstance.addToTeachers(Teacher.read(it))
            }
            //courseTeacherInstance.teacherId = courseTeacherJson.teachers.id
            if(courseTeacherInstance.validate()){
                courseTeacherService.save(courseTeacherInstance)
                resultMap = [messageType:"info", message:"İşlem başarıyla gerçekleşti."]
            }else{
                resultMap = [messageType:"error", message:"İşlem başarısız."]
            }
        }else{
            resultMap = [messageType:"error", message:"İşlem başarısız!!."]
        }
        */
        resultMap as JSON
    }
}

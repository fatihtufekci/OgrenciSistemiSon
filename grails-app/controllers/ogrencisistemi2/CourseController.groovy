package com.fatih

import grails.converters.JSON
import ogrencisistemi2.CourseService
import ogrencisistemi2.CourseTeacherService
import ogrencisistemi2.PersonService

class CourseController {

    CourseService courseService
    CourseTeacherService courseTeacherService
    PersonService personService

    def index() {}
    def addCourse(){
        def courseJson = request.JSON.courseInstance
        //courseJson.courseTeachers = null
        def courseInstance
        def resultMap



        if(courseJson != null){
            courseInstance = new Course()
            courseInstance.properties = courseJson
            courseService.save(courseInstance)

            courseJson.courseTeachersid.each{
                def teacherInstance = Teacher.read(it)
                def courseTeacherIns=new CourseTeacher()
                courseTeacherIns.course=courseInstance
                courseTeacherIns.teacher=teacherInstance
                courseTeacherService.save(courseTeacherIns)
            }
                if(courseInstance.errors?.errorCount > 0){
                    resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
                }else{
                    resultMap = [messageType:"info", message:"İşlem gerçekleşti.."]
                }

        }else{
            resultMap = [messageType:"error", message:"İşlem gerçekleşmedi.."]
        }

        render resultMap as JSON
    }

    def merhaba(){}

    def getcourselist(){
        def liste = courseService.list(params)
        render liste as JSON
    }

}

package ogrencisistemi2

import grails.converters.JSON

class DepartmentController {

    DepartmentService departmentService

    def index() { }

    def getir(){
        def liste = departmentService.list(params)
        render liste as JSON
    }

}

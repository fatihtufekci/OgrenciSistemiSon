package ogrencisistemi2

import com.fatih.auth.User
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class AnasayfaController {
    def springSecurityService

    def index() {
        def currentUser=(User)springSecurityService.getCurrentUser()
        if(currentUser != null){
            if(currentUser.usertype.usertypeName.equals("Admin")){
            }else if(currentUser.usertype.id==2){
                redirect(controller:"studenthome", action:"index")
            }else if(currentUser.usertype.usertypeName.equals("Staff")){
                //redirect(controller:"anasayfa", action:"index")
            }else if(currentUser.usertype.usertypeName.equals("Teacher")){
                redirect(controller:"teacher", action:"index")
            }
        }
    }

    def deneme2(){

    }

    //@Secured(['ROLE_USER'])
    def deneme(){
/*        def user = User.get(springSecurityService.principal.id)

        def posts = []
        if (user.following) {
            posts = Post.withCriteria {
                'in'("user", user.following)
                order("createdOn", "desc")
            }
        }
        [ posts: posts, postCount: posts.size() ]*/

        def currentUser=(User)springSecurityService.getCurrentUser()
        def username = currentUser.getUsername()
        def resultMap = [message:"Başarılı", username:username]
        render resultMap as JSON
    }

    def deneme3(){

    }

}

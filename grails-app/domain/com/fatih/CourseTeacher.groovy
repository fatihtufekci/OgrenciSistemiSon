package com.fatih

class CourseTeacher implements Serializable {

    String year

    // belongsTo'ya courseStudent'a da ekleyebilirsin.
    static belongsTo = [course: Course, teacher: Teacher, courseStudent:CourseStudent]

    static mapping = {
        id composite: ['course', 'teacher']
    }

    static constraints = {
        courseStudent nullable: true
        year nullable:true
    }
}

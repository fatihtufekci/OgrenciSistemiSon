package com.fatih

class Staff{

    int id
    Long staffNo
    Long sgkNo
    int  staffSalary

    static constraints = {
        staffNo nullable: false
        staffSalary nullable:false
    }
}

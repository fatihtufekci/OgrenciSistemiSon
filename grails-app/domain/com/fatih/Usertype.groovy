package com.fatih

class Usertype {

    int id
    String usertypeName

    static constraints = {
        usertypeName nullable: false
    }
}

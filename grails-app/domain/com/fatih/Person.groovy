package com.fatih

class Person {

    int id
    String firstName
    String lastName
    Long tckn
    Long phoneNumber

    static belongsTo = [student:Student, staff:Staff, teacher:Teacher]

    static constraints = {
        firstName blank:false, nullable: false
        lastName blank:false, nullable:false
        tckn blank:false, nullable:false
        phoneNumber blank:true, nullable:true
        student nullable: true
        staff nullable: true
        teacher nullable: true
    }

    def convertToMap(){
        def resultMap = [id:this.id
                         , firstName:this.firstName
                         , lastName:this.lastName
                         , tckn:this.tckn
                         , phoneNumber:this.phoneNumber
                         , student:this.student?[id:this.student.id.toString(),studentGhostNo:this.student.studentNo,
                                                 studentGhostClasses:this.student.classes,
                                                 studentGhostDepartmentName:this.student.department.departmentName,
                                                 studentGhostDepartmentId:this.student.department.id?.toString()]:null
                         , staff:this.staff?[id:this.staff.id.toString(),staffGhostName:this.staff.staffNo ]:null
                         , teacher:this.teacher?[id:this.teacher.id.toString(),teacherGhostEducationLevel:this.teacher.educationLevel,
                                                 teacherGhostDepartmentName:this.teacher.department.departmentName,
                                                 teacherGhostDepartmentId:this.teacher.department.id?.toString()]:null
        ]
        return resultMap
    }
}

package com.fatih

class CourseStudent {

    int id
    int csDiscontinuity
    int csResultsofExam
    static belongsTo = [course:Course, student:Student]

    /*static mapping = {
        id composite: ['course', 'teacher']
    }*/

    static constraints = {
        csDiscontinuity nullable:true
        csResultsofExam nullable:true
    }

    def convertToMap() {
        def resultMap = [id          : this.id
                         , csDiscontinuity: this.csDiscontinuity
                         , csResultsofExam: this.csResultsofExam
                         , student:this.student?[id:this.student.id.toString(),studentGhostNo:this.student.studentNo,
                                                 studentGhostClasses:this.student.classes]:null
                         , course:this.course?[id: this.course.id.toString(), courseGhostName: this.course.courseName,
                                                  courseGhostDepartmentName:this.course.department.departmentName] : null
        ]
        return resultMap
    }
}

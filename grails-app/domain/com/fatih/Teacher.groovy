package com.fatih

class Teacher {

    int id
    String educationLevel

    static belongsTo = [department:Department]

    static hasMany = [courseTeachers: CourseTeacher]

    static constraints = {
        department nullable: true
        courseTeachers nullable: true
    }

    def convertToMap(){
        def resultMap = [id:this.id
                         ,educationLevel:this.educationLevel
                         , department:this.department?[id:this.department.id.toString(),departmentGhostName:this.department.departmentName ]:null
        ]
        return resultMap
    }
}

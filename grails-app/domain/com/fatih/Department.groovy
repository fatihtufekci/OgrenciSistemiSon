package com.fatih

class Department {

    int id
    String departmentName

    static hasMany = [announcements:Announcement]

    static constraints = {
        departmentName blank: false, nullable: false
    }

    String toString() {
        return departmentName
    }
}

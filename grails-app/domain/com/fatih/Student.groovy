package com.fatih

class Student {

    int id
    Long studentNo
    int classes

    static belongsTo = [department:Department]

    static hasMany = [courseStudents: CourseStudent]

    static constraints = {
        department nullable: true
        courseStudents nullable: true
    }
    def convertToMap(){
        def resultMap = [id:this.id
                         , studentNo: this.studentNo
                         , classes: this.classes
                         , department:this.department?[id:this.department.id.toString(),departmentGhostName:this.department.departmentName]:null
        ]
        return resultMap
    }
}

package com.fatih

class Course {

    int id
    String courseName

    static belongsTo = [department: Department, type: Type, term: Term, status: Status]

    static hasMany = [courseTeachers: CourseTeacher, courseStudents: CourseStudent]

    static constraints = {
        courseName blank: false, nullable: false
        department nullable: false
        type nullable: false
        term nullable: false
        status nullable: false
        courseStudents nullable: true
        courseTeachers nullable: true
    }

    def convertToMap() {
        def resultMap = [id          : this.id
                         , courseName: this.courseName
                         , department: this.department ? [id: this.department.id.toString(), departmentGhostName: this.department.departmentName] : null
                         , type      : this.type ? [id: this.type.id.toString(), typeGhostName: this.type.typeName] : null
                         , term      : this.term ? [id: this.term.id.toString(), termGhostName: this.term.termName] : null
                         , status    : this.status ? [id: this.status.id.toString(), statusGhostName: this.status.statusName] : null
        ]
        return resultMap
    }

}

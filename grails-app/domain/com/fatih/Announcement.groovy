package com.fatih

class Announcement {

    int id
    String annoTitle
    String annoContent
    boolean annoIsActive

    // Person type..
    //String author

    Date releaseDate
    Date dateRemovedFromBroadcast

    static belongsTo = [Department]

    static hasMany = [departments:Department]

    static constraints = {
        departments nullable: true
        annoIsActive nullable: true
    }

    //static mapping = {
      //  departments cascade: "all-delete-orphan"
    //}

    def convertToMap(){
        def departmentList = this.departments.collect{it.id.toString()}
        def resultMap = [id:this.id
        ,annoTitle:this.annoTitle
        ,annoContent:this.annoContent
        ,annoIsActive:this.annoIsActive
        ,releaseDate:this.releaseDate?.format("yyyy.MM.dd")
        ,dateRemovedFromBroadcast:this.dateRemovedFromBroadcast?.format("yyyy.MM.dd")
        ,departments: departmentList

        ]
        return resultMap
    }
}

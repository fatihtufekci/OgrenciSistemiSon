package ogrencisistemi2

import com.fatih.CourseTeacher
import grails.gorm.services.Service

@Service(CourseTeacher)
interface CourseTeacherService {

    CourseTeacher get(Serializable id)

    List<CourseTeacher> list(Map args)

    Long count()

    void delete(Serializable id)

    CourseTeacher save(CourseTeacher courseTeacher)

    CourseTeacher getAll()

}

package ogrencisistemi2

import com.fatih.CourseStudent
import grails.gorm.services.Service

@Service(CourseStudent)
interface CourseStudentService {

    CourseStudent get(Serializable id)

    List<CourseStudent> list(Map args)

    Long count()

    void delete(Serializable id)

    CourseStudent save(CourseStudent courseStudent)
}

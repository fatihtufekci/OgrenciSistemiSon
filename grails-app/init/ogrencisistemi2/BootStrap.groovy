package ogrencisistemi2

import com.fatih.Usertype
import com.fatih.auth.Requestmap
import com.fatih.auth.Role
import com.fatih.auth.User
import com.fatih.auth.UserRole
import org.hibernate.usertype.UserType

class BootStrap {

    def init = { servletContext ->

        def admin = Role.findByAuthority("ROLE_ADMIN")
        def user = Role.findByAuthority("ROLE_USER")
        if(!admin && !user){
            admin = new Role(authority: "ROLE_ADMIN")
            admin.save()
            user = new Role(authority: "ROLE_USER")
            user.save()
        }

        def typeadmin = Usertype.findById(1)
        def typestudent = Usertype.findById(2)
        def typestaff = Usertype.findById(3)
        def typeteacher = Usertype.findById(4)


        if(!User.findByUsername("fatih") && !User.findByUsername("yunus") ){
            def u = new User(username: "fatih", password: "secret", usertype:typeadmin).save()
            def u2 = new User(username: "yunus", password: "1234", usertype:typestudent).save()

            UserRole.create(u, admin, false)
            UserRole.create(u, user, false)
            UserRole.create(u2, user, false)
        }

        if(!User.findByUsername("galata")){
            def user1 = new User(username: "galata", password: "1234", usertype:typestudent).save()
            UserRole.create(user1, admin, false)
        }

        //new Requestmap(url:"/main/adminPage", configAttribute: "ROLE_ADMIN").save()
        //new Requestmap(url:"/**", configAttribute: "IS_AUTHENTICATED_ANONYMOUSLY").save()

    }
    def destroy = {
    }
}

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Student Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
    body {font-family: Arial, Helvetica, sans-serif;}

    /* Full-width input fields */
    input[type=text], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    /* Set a style for all buttons */
    button {
        background-color: #4CAF50;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    button:hover {
        opacity: 0.8;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #f44336;
    }

    /* Center the image and position the close button */
    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
        position: relative;
    }

    .container {
        padding: 16px;
    }

    span.psw {
        float: right;
        padding-top: 16px;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        padding-top: 60px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
        border: 1px solid #888;
        width: 80%; /* Could be more or less, depending on screen size */
    }

    /* The Close Button (x) */
    .close {
        position: absolute;
        right: 25px;
        top: 0;
        color: #000;
        font-size: 35px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: red;
        cursor: pointer;
    }

    /* Add Zoom Animation */
    .animate {
        -webkit-animation: animatezoom 0.6s;
        animation: animatezoom 0.6s
    }

    @-webkit-keyframes animatezoom {
        from {-webkit-transform: scale(0)}
        to {-webkit-transform: scale(1)}
    }

    @keyframes animatezoom {
        from {transform: scale(0)}
        to {transform: scale(1)}
    }

    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
        span.psw {
            display: block;
            float: none;
        }
        .cancelbtn {
            width: 100%;
        }
    }
    </style>
</head>
<body>

<div ng-app="myApp" ng-controller="CourseController as myVm">

    Show: <input type="checkbox" ng-model="checked" aria-label="Toggle ngShow"><br />
    <div class="check-element animate-show-hide" ng-show="checked">I show up when your checkbox is checked.
    </div>

<h2 ng-show="checked">Personel Bilgileri</h2>

<button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Add Course</button>



<div id="id01" class="modal">

    <form class="modal-content animate">
        <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

        <div class="container">

            <h2 class="form-signin-heading">Add a Course</h2><br/>
            <label for="courseName" class="sr-only">Course Name</label>
            <input type="text" ng-model="myVm.courseInstance.courseName" value="{{myVm.courseInstance.courseName}}" id="courseName" class="form-control" placeholder="Course Name" required autofocus>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Type</label>
                <select  class="form-control" ng-model="myVm.courseInstance.type.id" >
                    <option ng-repeat="x in myVm.type track by $index"  value="{{x.id}}">{{x.typeName}}</option>
                </select>
            </div>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Status</label>
                <select class="form-control" ng-model="myVm.courseInstance.status.id">
                    <option ng-repeat="x in myVm.status track by $index" value="{{x.id}}">{{x.statusName}}</option>
                </select>
            </div>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Term</label>
                <select class="form-control" ng-model="myVm.courseInstance.term.id" >
                    <option ng-repeat="x in myVm.term track by $index" value="{{x.id}}">{{x.termName}}</option>
                </select>
            </div>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
                <select class="form-control" ng-model="myVm.courseInstance.department.id" >
                    <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                </select>
            </div>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Teacher</label>
                <select multiple class="form-control" ng-model="myVm.courseInstance.courseTeachersid" >
                    <option ng-repeat="x in myVm.teachers track by $index" value="{{x.id}}">{{x.firstName+" "+ x.lastName}}</option>
                </select>
            </div><br/><br/>

            <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Sign in</button>

        </div> <!-- /container -->

        <div class="container" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
    </form>
</div>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>

</body>
</html>

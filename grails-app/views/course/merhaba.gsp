<!DOCTYPE html>
<html>
<head>
    <script src="http://code.angularjs.org/1.2.0-rc.2/angular.js"></script>
    <script>
        var cartApp = angular.module('sCartApp', []);
        cartApp.controller('sCartCtrl', function($scope) {
            var myVm = this;
            myVm.ngShowhideFun = ngShowhideFun;
            myVm.buton1Action = buton1Action;

            myVm.buton1 = false;
            
            function buton1Action(flag){
              if(flag){
                  myVm.buton1 = false;
                  myVm.ngShowhide = false;
              }else{
                  myVm.buton1 = true;
              }
            }


            myVm.ngShowhide = false;
            function ngShowhideFun(flag){
                if (flag) {
                    myVm.ngShowhide = false;

                    myVm.buton1 = false;
                } else {
                    myVm.ngShowhide = true;
                }
            }
        });
    </script>
</head>
<body ng-app="sCartApp">
<div ng-controller="sCartCtrl as myVm">
    <a ng-click="myVm.ngShowhideFun(myVm.ngShowhide)">angularjs toggle show hide button</a>
    <button ng-click="myVm.buton1Action(myVm.buton1)">Butona bas</button>


    <div ng-show="myVm.ngShowhide">
        <div style=" background-color : yellow">
            Hi This is body area!
        </div>
    </div>

    <div ng-show="myVm.buton1">
        <div><p style="font-size: 20px; color: #4CAF50">FATİH</p></div>
    </div>
</div>
</body>

</html>
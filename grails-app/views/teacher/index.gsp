<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Teacher Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>

<div ng-app="myApp2" ng-controller="StudenthomeController as myVm">

    <legend><h4>Course List</h4></legend>
    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <th scope="col" >Course Name</th>
            <th scope="col" >Department Name</th>
            <th scope="col" >Term Name</th>
            <th scope="col" >Type Name</th>
            <th scope="col" >Status Name</th>
            <th style="width:200px;"></th>
        </tr>

        </thead>
        <tbody>
        <tr ng-repeat="x in myVm.courses | filter: filtrele">
            <td><span style="font-size: 18px">{{x.courseName}}</span></td>
            <td><span style="font-size: 18px">{{x.department.departmentGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.term.termGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.type.typeGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.status.statusGhostName}}</span></td>
        </tr>
        </tbody>
    </table>

</div>

</body>
</html>
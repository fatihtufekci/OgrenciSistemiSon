<%@ page import="com.fatih.Department" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Admin Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div ng-app="myApp2">
<div ng-controller="AdminController as myVm">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item active">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" ng-click="myVm.btnCourseAction(myVm.btnCourse)">Course</button>
                </li>
                <li class="nav-item active">
                    <a class="btn btn-outline-success my-2 my-sm-0"  ng-click="myVm.btnStudentAction(myVm.btnStudent)">Student</a>
                </li>
                <li class="nav-item active">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" ng-click="myVm.btnTeacherAction(myVm.btnTeacher)">Teacher</button>
                </li>
                <li class="nav-item active">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" ng-click="myVm.btnAnnoAction(myVm.btnAnnouncement)">Announcement</button>
                </li>
                <li class="nav-item active">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Staff</button>
                </li>
            </ul>
        </div>
    </nav>

    <div ng-show="myVm.btnEditanno">
        <h2 class="form-signin-heading">Update Announcement</h2><br/>
        <input type="text" ng-model="myVm.annoInstance.annoTitle" value="myVm.annoInstance.annoTitle" id="title" class="form-control" placeholder="Announcement Title" required autofocus>
        <br/>
        <input type="text" ng-model="myVm.annoInstance.annoContent" value="myVm.annoInstance.annoContent" id="content" class="form-control" placeholder="Announcement Content">
        <br/>
        <br/>
        <label>Is Active</label>
        <input type="checkbox" ng-model="myVm.annoInstance.annoIsActive" value="myVm.annoInstance.annoIsActive" id="isActive" class="check-box">
        <br/>
        <br/>
        <label>Release Date</label>
        <input type="date" ng-model="myVm.annoInstance.releaseDate" value="myVm.annoInstance.releaseDate" id="StphoneNumber" class="form-control">
        <br/>
        <label>Date Removed From Broadcast</label>
        <input type="date" ng-model="myVm.annoInstance.dateRemovedFromBroadcast" value="myVm.annoInstance.dateRemovedFromBroadcast" id="staffNo" class="form-control">
        <br/>
        <br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Department</label>
            <select multiple class="form-control" ng-model="myVm.annoInstance.departments">
                <option ng-repeat="x in myVm.departments track by $index" value="{{x.id}}">{{x.departmentName}}</option>
            </select>
        </div>
        <input type="hidden" ng-model="myVm.annoInstance.id" value="{{myVm.annoInstance.id}}">
        <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.editanno()">Update</button>
    </div>

    <div ng-show="myVm.btnAnnouncement">
        <h1 class="span-24 last">Announcement</h1>
        <div class="span-20">
            <ul ng-repeat="x in myVm.announcement">
                <li><span style="font-size: 18px">{{x.annoTitle}}</span></li>
                <li>{{x.annoContent}}</li>
                <li>{{x.releaseDate}}</li>
                <li>{{x.dateRemovedFromBroadcast}}</li>
                <ul ng-repeat="y in x.departments">
                    <li>{{y.departmentName}}</li>
                </ul>
                <li><button class="btn btn-outline-warning" type="button" ng-click="myVm.getannoinfo(x.id, myVm.btnEditanno)">Edit</button>
                    <button class="btn btn-outline-warning" type="button" ng-click="myVm.deleteanno(x.id)">Delete</button></li>
            </ul>
        </div>
    </div>

    <div class="container" ng-show="myVm.btnEditTeacher">
        <h2 class="form-signin-heading">Update Teacher</h2><br/>
        <label for="firstname" class="sr-only">Teacher Firstname</label>
        <input type="text" ng-model="myVm.teacherInstance.firstName" value="{{myVm.teacherInstance.firstName}}" id="firstname" class="form-control" placeholder="TeacherFirstname" required autofocus>
        <br/>
        <label for="lastname" class="sr-only">Teacher Lastname</label>
        <input type="text" ng-model="myVm.teacherInstance.lastName" value="{{myVm.teacherInstance.lastName}}" id="lastname" class="form-control" placeholder="Teacher Lastname">
        <br/>
        <label for="tckn" class="sr-only">Teacher TCKN</label>
        <input type="text" ng-model="myVm.teacherInstance.tckn" value="{{myVm.teacherInstance.tckn}}" id="tckn" class="form-control" placeholder="Teacher TCKN">
        <br/>
        <label for="phoneNumber" class="sr-only">Teacher PhoneNumber</label>
        <input type="text" ng-model="myVm.teacherInstance.phoneNumber" value="{{myVm.teacherInstance.phoneNumber}}" id="phoneNumber" class="form-control" placeholder="Teacher PhoneNumber">
        <br/>
        <label for="educationLevel" class="sr-only">Teacher EducationLevel</label>
        <input type="text" ng-model="myVm.teacherInstance.teacher.teacherGhostEducationLevel" value="{{myVm.teacherInstance.teacher.teacherGhostEducationLevel}}" id="educationLevel" class="form-control" placeholder="Teacher EducationLevel">
        <br/>
        <br/>
        <div class="form-group">
            <label>Course Department</label>
            <select class="form-control" ng-model="myVm.teacherInstance.teacher.teacherGhostDepartmentId">
                <option ng-repeat="x in myVm.departments track by $index" value="{{x.id}}">{{x.departmentName}}</option>
            </select>
            <g:select name="departmant" from="${com.fatih.Department.list()}" value="{{myVm.teacherInstance.teacher.teacherGhostDepartmentId}}"  ng-model="myVm.teacherInstance.teacher.teacherGhostDepartmentId"/>
        </div>
        <br/>
        <input type="hidden" ng-model="myVm.teacherInstance.id" value="{{myVm.teacherInstance.id}}">
        <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.editteacher()">Update</button>
    </div>

    <div ng-show="myVm.btnTeacher">
        <legend><h4>Teacher List</h4></legend>
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col" >Teacher First Name</th>
                <th scope="col" >Teacher Last Name</th>
                <th scope="col" >Teacher TCKN</th>
                <th scope="col" >Teacher Phonenumber</th>
                <th scope="col" >Teacher EducationLevel</th>
                <th scope="col" >Teacher Department</th>
                <th style="width:200px;"></th>
            </tr>

            </thead>
            <tbody>
            <tr ng-repeat="x in myVm.personTeacher | filter: filtrele">
                <td><span style="font-size: 18px">{{x.firstName}}</span></td>
                <td><span style="font-size: 18px">{{x.lastName}}</span></td>
                <td><span style="font-size: 18px">{{x.tckn}}</span></td>
                <td><span style="font-size: 18px">{{x.phoneNumber}}</span></td>
                <td><span style="font-size: 18px">{{x.teacher.teacherGhostEducationLevel}}</span></td>
                <td><span style="font-size: 18px">{{x.teacher.teacherGhostDepartmentName}}</span></td>
                <form method="post">
                    <td>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.getteacherinfo(x.id,myVm.btnEditTeacher)">Edit</button>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.deleteteacher(x.id)">Delete</button>
                    </td>

                </form>
            </tr>
            </tbody>
        </table>
    </div>

    <div ng-show="myVm.btnEditStudent">
        <h2 class="form-signin-heading">Update Student</h2><br/>
        <input type="text" ng-model="myVm.studentInstance.firstName" value="myVm.studentInstance.firstName" id="Sfirstname" class="form-control" placeholder="Student Firstname" required autofocus>
        <br/>
        <input type="text" ng-model="myVm.studentInstance.lastName" value="myVm.studentInstance.lastName" id="Slastname" class="form-control" placeholder="Student Lastname">
        <br/>
        <input type="text" ng-model="myVm.studentInstance.tckn" value="myVm.studentInstance.tckn" id="Stckn" class="form-control" placeholder="Student TCKN">
        <br/>
        <input type="text" ng-model="myVm.studentInstance.phoneNumber" value="myVm.studentInstance.phoneNumber" id="SphoneNumber" class="form-control" placeholder="Student PhoneNumber">
        <br/>
        <input type="text" ng-model="myVm.studentInstance.student.studentGhostNo" value="myVm.studentInstance.student.studentGhostNo" id="studentNo" class="form-control" placeholder="Student No">
        <br/>
        <input type="text" ng-model="myVm.studentInstance.student.studentGhostClasses" value="myVm.studentInstance.student.studentGhostClasses" id="studentClasses" class="form-control" placeholder="Student Classes">
        <br/>
        <br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Student Department</label>
            <select class="form-control" ng-model="myVm.studentInstance.student.studentGhostDepartmentId">
                <option ng-repeat="x in myVm.departments track by $index" value="{{x.id}}">{{x.departmentName}}</option>
            </select>
        </div>
        <br/>
        <input type="hidden" ng-model="myVm.studentInstance.id" value="{{myVm.studentInstance.id}}">
        <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.editstudent()">Update</button>
    </div>

    <div ng-show="myVm.btnStudent">
        <legend><h4>Student List</h4></legend>
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col" >Student First Name</th>
                <th scope="col" >Student Last Name</th>
                <th scope="col" >Student TCKN</th>
                <th scope="col" >Student Phonenumber</th>
                <th scope="col" >Student No</th>
                <th scope="col" >Student Department</th>
                <th style="width:200px;"></th>
            </tr>

            </thead>
            <tbody>
            <tr ng-repeat="x in myVm.personStudent | filter: filtrele">
                <td><span style="font-size: 18px">{{x.firstName}}</span></td>
                <td><span style="font-size: 18px">{{x.lastName}}</span></td>
                <td><span style="font-size: 18px">{{x.tckn}}</span></td>
                <td><span style="font-size: 18px">{{x.phoneNumber}}</span></td>
                <td><span style="font-size: 18px">{{x.student.studentGhostNo}}</span></td>
                <td><span style="font-size: 18px">{{x.student.studentGhostDepartmentName}}</span></td>
                <form method="post">
                    <td>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.getstudentinfo(x.id, myVm.btnEditStudent)">Edit</button>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.deletestudent(x.id)">Delete</button>
                    </td>

                </form>
            </tr>
            </tbody>
        </table>
    </div>



    <div ng-show="myVm.btnEditcourse">
        <h2 class="form-signin-heading">Update Course</h2><br/>
        <label for="courseName" class="sr-only">Course Name</label>
        <input type="text" ng-model="myVm.courseInstance.courseName" value="myVm.courseInstance.courseName" id="courseName" class="form-control" placeholder="Course Name" required autofocus>
        <br/><br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Type</label>
            <select  class="form-control" ng-model="myVm.courseInstance.type.id" value="myVm.courseInstance.type.id">
                <option ng-repeat="x in myVm.type track by $index"  value="{{x.id}}">{{x.typeName}}</option>
            </select>
        </div>
        <br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Status</label>
            <select class="form-control" ng-model="myVm.courseInstance.status.id">
                <option ng-repeat="x in myVm.status track by $index" value="{{x.id}}">{{x.statusName}}</option>
            </select>
        </div>
        <br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Term</label>
            <select class="form-control" ng-model="myVm.courseInstance.term.id" >
                <option ng-repeat="x in myVm.term track by $index" value="{{x.id}}">{{x.termName}}</option>
            </select>
        </div>
        <br/>
        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
            <select class="form-control" ng-model="myVm.courseInstance.department.id" >
                <option ng-repeat="x in myVm.departments track by $index" value="{{x.id}}">{{x.departmentName}}</option>
            </select>
        </div>
        <br/>
%{--        <div class="form-group">
            <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Teacher</label>
            <select multiple class="form-control" ng-model="myVm.courseInstance.courseTeachersid" >
                <option ng-repeat="x in myVm.personTeacher track by $index" value="{{x.teacher.id}}">{{x.firstName+" "+ x.lastName}}</option>
            </select>
        </div><br/>--}%
        <br/>
        <input type="hidden" ng-model="myVm.courseInstance.id" value="{{myVm.courseInstance.id}}">
        <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.editcourse()">Update</button>
    </div>


    <div ng-show="myVm.btnCourse">
        <legend><h4>Course List</h4></legend>
        <table class="table table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col" >Course Name</th>
                <th scope="col" >Department Name</th>
                <th scope="col" >Term Name</th>
                <th scope="col" >Type Name</th>
                <th scope="col" >Status Name</th>
                <th style="width:200px;"></th>
            </tr>

            </thead>
            <tbody>
            <tr ng-repeat="x in myVm.courses | filter: filtrele">
                <td><span style="font-size: 18px">{{x.courseName}}</span></td>
                <td><span style="font-size: 18px">{{x.department.departmentGhostName}}</span></td>
                <td><span style="font-size: 18px">{{x.term.termGhostName}}</span></td>
                <td><span style="font-size: 18px">{{x.type.typeGhostName}}</span></td>
                <td><span style="font-size: 18px">{{x.status.statusGhostName}}</span></td>
                <form method="post">
                    <td>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.getcourseinfo(x.id, myVm.btnEditcourse)">Edit</button>
                        <button class="btn btn-outline-warning" type="button" ng-click="myVm.deletecourse(x.id)">Delete</button>
                    </td>

                </form>
            </tr>
            </tbody>
        </table>
    </div>

</div>
</div>
</body>
</html>
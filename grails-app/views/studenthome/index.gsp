<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Student Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div ng-app="myApp2">
    <div ng-controller="StudenthomeController as myVm">
    <div class="container">
        <h1 class="span-24 last">Announcement</h1>

        <div class="span-20">
            <ul>
                <li ng-repeat="x in myVm.announcement">{{x.annoTitle}}</li>
            </ul>
        </div>
    </div>
    <br/>
    <legend>Search Course</legend>
    <div class="form-group">
        <form class="form-inline" method="post">
            <input type="search" ng-model="filtrele" style="font-size: 18px; font-weight: bold" placeholder="Course Name" class="form-control"/>
        </form>
    </div>
    <br/>


    <legend><h4>Select Course</h4></legend>
    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <th scope="col" >Course Name</th>
            <th scope="col" >Department Name</th>
            <th scope="col" >Term Name</th>
            <th scope="col" >Type Name</th>
            <th scope="col" >Status Name</th>
            <th style="width:200px;"></th>
        </tr>

        </thead>
        <tbody>
        <tr ng-repeat="x in myVm.courses | filter: filtrele">
            <td><span style="font-size: 18px">{{x.courseName}}</span></td>
            <td><span style="font-size: 18px">{{x.department.departmentGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.term.termGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.type.typeGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.status.statusGhostName}}</span></td>
            <form method="post">
                <td>
                    <button class="btn btn-outline-warning" type="button" ng-click="myVm.selectcourses(x.id)">Select</button>
                </td>

            </form>
        </tr>
        </tbody>
    </table>
        <br/><br/>
        <g:render template="mycourses"/>
    </div>
</div>



</body>
</html>
<div >
    <legend><h4>My Courses</h4></legend>
    <table class="table table-hover">
        <thead class="thead-dark">
        <tr>
            <th scope="col" >Course Name</th>
            <th scope="col" >Department Name</th>
            <th style="width:200px;"></th>
        </tr>

        </thead>
        <tbody>
        <tr ng-repeat="x in myVm.mycourses">
            <td><span style="font-size: 18px">{{x.course.courseGhostName}}</span></td>
            <td><span style="font-size: 18px">{{x.course.courseGhostDeparmentName}}</span></td>
        </tr>
        </tbody>
    </table>
</div>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>

<div id="content" role="main">

    <h2>Available Controllers:</h2>
    <ul class="list-group">
        <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
            <li class="list-group-item">
                <g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>
            </li>
        </g:each>
    </ul>


</div>

</body>
</html>

<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>
<body>
<div class="w3-container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">OBİS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">Admin Home</a>
                    </li>
                </sec:ifAnyGranted>

                <li class="nav-item">
                    <a class="nav-link" href="/anasayfa">Staff Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/studenthome" >Student Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/teacher">Teacher Home</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <sec:ifLoggedIn><li><g:link controller="logout"><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>&nbsp;&nbsp;</g:link></li></sec:ifLoggedIn>
                <sec:ifLoggedIn>
                    <li><button class="btn btn-outline-success my-2 my-sm-0"><sec:username/></button></li>
                </sec:ifLoggedIn>
%{--                <sec:ifLoggedIn>
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span>  <sec:username/></a></li>
                </sec:ifLoggedIn>--}%
            </ul>
        </div>
    </nav>

%{--<ul>
    <sec:ifLoggedIn>
        <li>
            <a href="#"  role="button">Hello <sec:username/><span class="caret"></span></span></a>
            <ul>
                <li><a href="#">Profile</a></li>
                <li><g:link controller="logout">Logout</g:link></li>
            </ul>
        </li>
    </sec:ifLoggedIn>
</ul>--}%


   %{-- <div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
                <ul class="nav navbar-nav navbar-right">
                    <g:pageProperty name="page.nav" />
                </ul>
            </div>

            <div class="navbar-collapse collapse" aria-expanded="false">
                <ul class="nav navbar-nav navbar-right">
                    <sec:ifLoggedIn>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello <sec:username/><span class="caret"></span></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li><g:link controller="logout">Logout</g:link></li>
                            </ul>
                        </li>
                    </sec:ifLoggedIn>
                </ul>
            </div>

        </div>
    </div>--}%

    <g:layoutBody/>

    <div class="footer" role="contentinfo"></div>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>



</div>
<asset:javascript src="application.js"/>

</body>
</html>

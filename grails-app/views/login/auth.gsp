
<!DOCTYPE html>
<html lang="en">
<head>
    %{--<meta name="layout" content="main"/>--}%
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h2>Please Login</h2>
    <form action="/login/authenticate" method="POST" id="loginForm" class="cssform" autocomplete="off">
        <div class="form-group">
            <label for="username">Email:</label>
            <input type="text" class="form-control" id="username" placeholder="Enter email" name="username">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
        </div>
        <div class="form-group form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="remember"> Remember me
            </label>
        </div>
        <button type="submit" id="submit" class="btn btn-primary">Submit</button>
    </form>
    <script>
        (function() {
            document.forms['loginForm'].elements['username'].focus();
        })();
    </script>

    <div id="spinner" class="spinner" style="display:none;">
        Loading&hellip;
    </div>
</div>

<script type="text/javascript" src="/assets/jquery-2.2.0.min.js?compile=false" ></script>
<script type="text/javascript" src="/assets/bootstrap.js?compile=false" ></script>
<script type="text/javascript" src="/assets/angular.js?compile=false" ></script>
<script type="text/javascript" src="/assets/course/app.js?compile=false" ></script>
<script type="text/javascript" src="/assets/application.js?compile=false" ></script>
</body>
</html>

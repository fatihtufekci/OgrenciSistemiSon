<div class="modal fade" id="teacher">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" ng-controller="TeacherController as myVm">
                <h2 class="form-signin-heading">Add a Teacher</h2><br/>
                <label for="firstname" class="sr-only">Teacher Firstname</label>
                <input type="text" ng-model="myVm.teacherInstance.firstName" id="firstname" class="form-control" placeholder="TeacherFirstname" required autofocus>
                <br/>
                <label for="lastname" class="sr-only">Teacher Lastname</label>
                <input type="text" ng-model="myVm.teacherInstance.lastName" id="lastname" class="form-control" placeholder="Teacher Lastname">
                <br/>
                <label for="tckn" class="sr-only">Teacher TCKN</label>
                <input type="text" ng-model="myVm.teacherInstance.tckn" id="tckn" class="form-control" placeholder="Teacher TCKN">
                <br/>
                <label for="phoneNumber" class="sr-only">Teacher PhoneNumber</label>
                <input type="text" ng-model="myVm.teacherInstance.phoneNumber" id="phoneNumber" class="form-control" placeholder="Teacher PhoneNumber">
                <br/>
                <label for="educationLevel" class="sr-only">Teacher EducationLevel</label>
                <input type="text" ng-model="myVm.teacherInstance.educationLevel" id="educationLevel" class="form-control" placeholder="Teacher EducationLevel">
                <br/>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
                    <select class="form-control" ng-model="myVm.teacherInstance.department.id" >
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

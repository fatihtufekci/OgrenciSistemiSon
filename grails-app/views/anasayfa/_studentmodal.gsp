<div class="modal fade" id="student">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" ng-controller="StudentController as myVm">
                <h2 class="form-signin-heading">Add a Student</h2><br/>
                <input type="text" ng-model="myVm.studentInstance.firstName" id="Sfirstname" class="form-control" placeholder="Student Firstname" required autofocus>
                <br/>
                <input type="text" ng-model="myVm.studentInstance.lastName" id="Slastname" class="form-control" placeholder="Student Lastname">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.tckn" id="Stckn" class="form-control" placeholder="Student TCKN">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.phoneNumber" id="SphoneNumber" class="form-control" placeholder="Student PhoneNumber">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.studentNo" id="studentNo" class="form-control" placeholder="Student No">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.classes" id="studentClasses" class="form-control" placeholder="Student Classes">
                <br/>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Student Department</label>
                    <select class="form-control" ng-model="myVm.studentInstance.department.id">
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
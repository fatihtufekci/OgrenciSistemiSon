<div class="modal fade" id="course">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" ng-controller="CourseController as myVm">
                <h2 class="form-signin-heading">Add a Course</h2><br/>
                <label for="courseName" class="sr-only">Course Name</label>
                <input type="text" ng-model="myVm.courseInstance.courseName" id="courseName" class="form-control"
                       placeholder="Course Name" required autofocus>
                <br/><br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Type</label>
                    <select  class="form-control" ng-model="myVm.courseInstance.type.id" >
                        <option ng-repeat="x in myVm.type track by $index"  value="{{x.id}}">{{x.typeName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Status</label>
                    <select class="form-control" ng-model="myVm.courseInstance.status.id">
                        <option ng-repeat="x in myVm.status track by $index" value="{{x.id}}">{{x.statusName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Term</label>
                    <select class="form-control" ng-model="myVm.courseInstance.term.id" >
                        <option ng-repeat="x in myVm.term track by $index" value="{{x.id}}">{{x.termName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
                    <select class="form-control" ng-model="myVm.courseInstance.department.id" >
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Teacher</label>
                    <select multiple class="form-control" ng-model="myVm.courseInstance.courseTeachersid" >
                        <option ng-repeat="x in myVm.teachers track by $index" value="{{x.teacher.id}}">{{x.firstName+" "+ x.lastName}}</option>
                    </select>
                </div><br/><br/>

                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




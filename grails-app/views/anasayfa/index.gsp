<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Staff Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<div ng-app="myApp">
    <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#announcement">Add Announcement</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#course">Add Course</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#teacher">Add Teacher</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#student">Add Student</a>
                    </li>

                    %{--<li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Add
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#course">Add Course</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#teacher">Add Teacher</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#student">Add Student</a>
                        </div>
                    </li>--}%
                </ul>
            </div>
        </nav>

    </div>

    <div class="container">
        <h3>Personel Anasayfa</h3>
        <p></p>
    </div>


    <g:render template="anno"/>
    <g:render template="studentmodal"/>
    <g:render template="teachermodal"/>
    <g:render template="coursemodal"/>
</div>

</body>
</html>

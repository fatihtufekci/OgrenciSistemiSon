<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Staff Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <style>
    /* Make the image fully responsive */
    .carousel-inner img {
        width: 100%;
        height: 100%;
    }
    </style>
</head>
<body>

<div id="demo" class="carousel slide" data-ride="carousel">
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
    </ul>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <g:img dir="images" file="zaim.jpg" width="1000" height="500"/>
            <div class="carousel-caption">
                <h5 style="color: #FF0000">Doğa ile iç içe</h5>
            </div>
        </div>
        <div class="carousel-item">
            <g:img dir="images" file="zaim2.jpg" width="1000" height="500"/>
            <div class="carousel-caption">
                <h5>Kent içinde kampüs üniversitesi</h5>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>

<div class="container" ng-app="myApp">
    <h2>Staff Home Page</h2>
    <br/>
    <!-- Button to Open the Modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#course">
        Add Course
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#student">
        Add Student
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#teacher">
        Add Teacher
    </button>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#announcement">
        Add Announcement
    </button>

    <!-- The Modal -->
    <div class="modal fade" id="course">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add Course</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" ng-controller="CourseController as myVm">

                <h2 class="form-signin-heading">Add a Course</h2><br/>
                <label for="courseName" class="sr-only">Course Name</label>
                <input type="text" ng-model="myVm.courseInstance.courseName" id="courseName" class="form-control" placeholder="Course Name" required autofocus>
                <br/><br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Type</label>
                    <select  class="form-control" ng-model="myVm.courseInstance.type.id" >
                        <option ng-repeat="x in myVm.type track by $index"  value="{{x.id}}">{{x.typeName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Status</label>
                    <select class="form-control" ng-model="myVm.courseInstance.status.id">
                        <option ng-repeat="x in myVm.status track by $index" value="{{x.id}}">{{x.statusName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Term</label>
                    <select class="form-control" ng-model="myVm.courseInstance.term.id" >
                        <option ng-repeat="x in myVm.term track by $index" value="{{x.id}}">{{x.termName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
                    <select class="form-control" ng-model="myVm.courseInstance.department.id" >
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Teacher</label>
                    <select multiple class="form-control" ng-model="myVm.courseInstance.courseTeachersid" >
                        <option ng-repeat="x in myVm.teachers track by $index" value="{{x.id}}">{{x.firstName+" "+ x.lastName}}</option>
                    </select>
                </div><br/><br/>

                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>

                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="student">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add Student</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" ng-controller="StudentController as myVm">

                <h2 class="form-signin-heading">Add a Student</h2><br/>
                <input type="text" ng-model="myVm.studentInstance.firstName" id="Sfirstname" class="form-control" placeholder="Student Firstname" required autofocus>
                <br/>
                <input type="text" ng-model="myVm.studentInstance.lastName" id="Slastname" class="form-control" placeholder="Student Lastname">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.tckn" id="Stckn" class="form-control" placeholder="Student TCKN">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.phoneNumber" id="SphoneNumber" class="form-control" placeholder="Student PhoneNumber">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.studentNo" id="studentNo" class="form-control" placeholder="Student No">
                <br/>
                <input type="text" ng-model="myVm.studentInstance.classes" id="studentClasses" class="form-control" placeholder="Student Classes">
                <br/>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Student Department</label>
                    <select class="form-control" ng-model="myVm.studentInstance.department.id">
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>

            </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="teacher">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add Teacher</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" ng-controller="TeacherController as myVm">

                <h2 class="form-signin-heading">Add a Teacher</h2><br/>
                <label for="firstname" class="sr-only">Teacher Firstname</label>
                <input type="text" ng-model="myVm.teacherInstance.firstName" id="firstname" class="form-control" placeholder="TeacherFirstname" required autofocus>
                <br/>
                <label for="lastname" class="sr-only">Teacher Lastname</label>
                <input type="text" ng-model="myVm.teacherInstance.lastName" id="lastname" class="form-control" placeholder="Teacher Lastname">
                <br/>
                <label for="tckn" class="sr-only">Teacher TCKN</label>
                <input type="text" ng-model="myVm.teacherInstance.tckn" id="tckn" class="form-control" placeholder="Teacher TCKN">
                <br/>
                <label for="phoneNumber" class="sr-only">Teacher PhoneNumber</label>
                <input type="text" ng-model="myVm.teacherInstance.phoneNumber" id="phoneNumber" class="form-control" placeholder="Teacher PhoneNumber">
                <br/>
                <label for="educationLevel" class="sr-only">Teacher EducationLevel</label>
                <input type="text" ng-model="myVm.teacherInstance.educationLevel" id="educationLevel" class="form-control" placeholder="Teacher EducationLevel">
                <br/>
                <br/>
                <div class="form-group">
                    <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Course Department</label>
                    <select class="form-control" ng-model="myVm.teacherInstance.department.id" >
                        <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                    </select>
                </div>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>


            </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="announcement">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Add Announcement</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" ng-controller="AnnouncementController as myVm">

                <g:render template="anno"/>

            </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

</div>
<br/>
<br/>

</body>
</html>

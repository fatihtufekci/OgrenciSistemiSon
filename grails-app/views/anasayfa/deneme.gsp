
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main"/>
    <title>Deneme</title>
</head>

<body>
<div class="container">

    <sec:ifNotLoggedIn>
        <g:link controller="login" action="auth">Login</g:link>
    </sec:ifNotLoggedIn>
    <sec:ifAllGranted roles="ROLE_USER">
        <g:link class="create" controller="anasayfa" action="index">My Timeline</g:link>
    </sec:ifAllGranted>

    <sec:username /> (<g:link controller="logout">sign out</g:link>)
</div>
</body>
</html>

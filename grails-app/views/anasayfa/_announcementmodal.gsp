<div id="announcementModal" class="modal">
    <form class="modal-content animate">
        <span onclick="document.getElementById('announcementModal').style.display='none'" class="close" title="Close Modal">&times;</span>
        <div class="container" ng-controller="AnnouncementController as myVm">
            <h2 class="form-signin-heading">Add a Announcement</h2><br/>
            <input type="text" ng-model="myVm.annoInstance.annoTitle" id="title" class="form-control" placeholder="Announcement Title" required autofocus>
            <br/>
            <input type="text" ng-model="myVm.annoInstance.annoContent" id="content" class="form-control" placeholder="Announcement Content">
            <br/>
            <br/>
            <label>Is Active</label>
            <input type="checkbox" ng-model="myVm.annoInstance.annoIsActive" id="isActive" class="check-box">
            <br/>
            <br/>
            <label>Release Date</label>
            <input type="date" ng-model="myVm.annoInstance.releaseDate" id="StphoneNumber" class="form-control">
            <br/>
            <label>Date Removed From Broadcast</label>
            <input type="date" ng-model="myVm.annoInstance.dateRemovedFromBroadcast" id="staffNo" class="form-control">
            <br/>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Department</label>
                <select multiple class="form-control" ng-model="myVm.annoInstance.departments">
                    <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                </select>
            </div>
            <button onclick="document.getElementById('announcementModal').style.display='none'" class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>
        </div>
        <div class="container" style="background-color:#f1f1f1">
            <button type="button" onclick="document.getElementById('announcementModal').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
    </form>
</div>
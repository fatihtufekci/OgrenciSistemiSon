<div class="modal fade" id="announcement">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body" ng-controller="AnnouncementController as myVm">

            <h2 class="form-signin-heading">Add a Announcement</h2><br/>
            <input type="text" ng-model="myVm.annoInstance.annoTitle" id="title" class="form-control" placeholder="Announcement Title" required autofocus>
            <br/>
            <input type="text" ng-model="myVm.annoInstance.annoContent" id="content" class="form-control" placeholder="Announcement Content">
            <br/>
            <br/>
            <label>Is Active</label>
            <input type="checkbox" ng-model="myVm.annoInstance.annoIsActive" id="isActive" class="check-box">
            <br/>
            <br/>
            <label>Release Date</label>
            <input type="date" ng-model="myVm.annoInstance.releaseDate" id="StphoneNumber" class="form-control">
            <br/>
            <label>Date Removed From Broadcast</label>
            <input type="date" ng-model="myVm.annoInstance.dateRemovedFromBroadcast" id="staffNo" class="form-control">
            <br/>
            <br/>
            <div class="form-group">
                <label style="font-size: 18px; font-weight: bold; font-family:'Arial'">Department</label>
                <select multiple class="form-control" ng-model="myVm.annoInstance.departments">
                    <option ng-repeat="x in myVm.department track by $index" value="{{x.id}}">{{x.departmentName}}</option>
                </select>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="button" ng-click="myVm.insert()">Create</button>

        </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

